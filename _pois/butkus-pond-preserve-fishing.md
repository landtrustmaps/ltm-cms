---
title: Butkus Pond Preserve Fishing
heading: Butkus Pond Preserve Fishing
icon: icon-fish
important: true
related: '27'
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Point","coordinates":[-73.257281,41.174148]}}]}
---
This hidden preserve in a quiet neighborhood contains a large "catch & release" pond with fathead minnows, bluegills, and some bass - perfect for fishing with children. 
