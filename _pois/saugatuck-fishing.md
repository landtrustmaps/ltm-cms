---
title: Saugatuck Reservoir Fishing
heading: Saugatuck Reservoir Fishing
icon: icon-fish
important: true
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Point","coordinates":[-73.365516,41.264877]}}]}
---
There is great fishing in the Saugatuck Reservoir. Learn more about the
[permitting process](https://www.aquarionwater.com/CT/fishing).
