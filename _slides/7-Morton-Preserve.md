---
title: Lillian Squires Morton Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.342234, 41.244102 ], [ -73.342206, 41.244097 ], [ -73.342179, 41.244089 ], [ -73.342154, 41.244077 ], [ -73.342132, 41.244063 ], [ -73.342114, 41.244046 ], [ -73.3421, 41.244027 ], [ -73.342091, 41.244006 ], [ -73.342086, 41.243985 ], [ -73.342063, 41.24394 ], [ -73.342032, 41.243897 ], [ -73.341996, 41.243858 ], [ -73.341953, 41.243822 ], [ -73.341904, 41.243791 ], [ -73.341851, 41.243764 ], [ -73.341794, 41.243742 ], [ -73.341734, 41.243726 ], [ -73.341672, 41.243715 ], [ -73.341231, 41.243678 ], [ -73.341179, 41.243657 ], [ -73.341055, 41.243565 ], [ -73.340924, 41.243204 ], [ -73.340942, 41.243144 ], [ -73.340908, 41.243083 ], [ -73.340864, 41.242922 ], [ -73.340762, 41.242644 ], [ -73.340922, 41.242497 ], [ -73.340969, 41.242472 ], [ -73.341073, 41.242542 ], [ -73.341181, 41.242608 ], [ -73.341295, 41.242668 ], [ -73.341413, 41.242724 ], [ -73.341536, 41.242774 ], [ -73.341662, 41.242819 ], [ -73.341791, 41.242859 ], [ -73.341923, 41.242892 ], [ -73.342057, 41.24292 ], [ -73.342194, 41.242942 ], [ -73.342331, 41.242957 ], [ -73.342313, 41.24303 ], [ -73.342312, 41.243036 ], [ -73.342312, 41.243048 ], [ -73.342312, 41.243067 ], [ -73.342312, 41.243068 ], [ -73.342313, 41.243086 ], [ -73.342313, 41.243087 ], [ -73.342314, 41.243105 ], [ -73.342314, 41.243106 ], [ -73.342315, 41.243124 ], [ -73.342315, 41.243125 ], [ -73.342318, 41.243144 ], [ -73.342318, 41.243149 ], [ -73.342323, 41.243183 ], [ -73.34233, 41.243241 ], [ -73.34233, 41.243243 ], [ -73.34234, 41.2433 ], [ -73.34234, 41.243301 ], [ -73.342355, 41.243372 ], [ -73.342356, 41.243373 ], [ -73.342377, 41.243453 ], [ -73.342392, 41.243532 ], [ -73.342411, 41.24363 ], [ -73.342425, 41.243703 ], [ -73.342438, 41.243774 ], [ -73.342438, 41.243776 ], [ -73.342457, 41.243851 ], [ -73.342474, 41.243914 ], [ -73.342474, 41.243916 ], [ -73.342498, 41.243989 ], [ -73.342522, 41.244066 ], [ -73.342542, 41.244142 ], [ -73.342234, 41.244102 ] ] ]}}]}
ltmid: '7'
region: weston
size: '3.6'
image: /dist/assets/17_LILLIAN_SQUIRES_MORTON_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/lillian-squires-morton-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.243127,-73.342365&q=8+Bradley+Hill+Road+Weston,+CT,+06883,+United+States&z=12
---
An open space gem thats at its best on a sunny day. The grassy path follows the perimeter of the preserve, which once was a pasture, a land improvement made by many early settlers, who cleared trees and rocks for a chance to raise something. The small pond (now home to a large snapping turtle) was a source of water for grazing livestock. A trail spur connects to the Freeborn Walk Trail which runs all the way to Old Redding Road and the Trout Brook Valley Preserve.



## Directions & Parking: 

North on Lyons Plain Road; at 3-way stop, bear left onto Valley Forge Road; about a mile and a half from the intersection, turn right on Bradley Road. Trail entrance is on the right. Park along the road.
