---
title: Ernest Hillman Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.288157,41.192919],[-73.288152,41.19291],[-73.287853,41.192429],[-73.287682,41.192241],[-73.287377,41.191895],[-73.287344,41.191857],[-73.287317,41.191827],[-73.286569,41.190958],[-73.287314,41.190617],[-73.287976,41.1903],[-73.288196,41.190583],[-73.288708,41.190322],[-73.28873,41.19031],[-73.288837,41.190256],[-73.288978,41.190196],[-73.289094,41.190325],[-73.288303,41.190691],[-73.28868,41.191148],[-73.2887,41.19117],[-73.288715,41.191187],[-73.289107,41.191641],[-73.28997,41.192668],[-73.289974,41.192672],[-73.28997,41.192673],[-73.289501,41.192799],[-73.289214,41.192876],[-73.28833,41.193113],[-73.288327,41.19311],[-73.28827,41.193046],[-73.288157,41.192919]]]}}]}
ltmid: '25'
region: fairfield
size: '11'
image: /dist/assets/30_ERNEST_HILLMAN_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/ernest-hillman-preserve/'
googlemaps: >-
  https://www.google.com/maps/place/300+Fair+Oak+Dr,+Fairfield,+CT+06824/@41.1724899,-73.3047248,19z/data=!3m1!4b1!4m5!3m4!1s0x89e80535b64b9a5b:0x873b9c44fc64f2ac!8m2!3d41.1724899!4d-73.3041776
categories:
  - category: hiking
---
Located at the end of High Point Lane, the Hillman Preserves 11 acres was donated by Ernest Hillman in 1988. This preserve consists of two lovely fields and woods and trails through the woodlands which provide a nice walk. In the front field, there is a memorial bench for Ann Carter who loved this preserve.

Ann, a long-time Land Trust Board Member and a leading proponent of Open Space in Fairfield was very disappointed when a new house appeared at the end of the field she loved at the preserve. She died in 2005, and many people made generous donations to the Land Trust in Ann's memory. With these funds, a split rail fence was built and thirty-five Norway Spruce trees were planted just behind the fence. As the trees grow, the view of the house will be blocked and the tranquility of the field will be restored. We know Ann would have liked this. The Hillman Preserve also provides access to the Lobdell Calf Pasture which is not trailed.


# Directions & Parking

The Ernest Hillman Preserve is located at the end of High Point Lane off Hillside Road. Park roadside at the cul-de-sac.
