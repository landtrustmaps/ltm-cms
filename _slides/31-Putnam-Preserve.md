---
title: Putnam & Rudkin Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.30854, 41.160896 ], [ -73.308502, 41.160845 ], [ -73.30789, 41.160057 ], [ -73.30788, 41.160044 ], [ -73.307874, 41.160035 ], [ -73.307774, 41.159898 ], [ -73.307762, 41.159881 ], [ -73.30763, 41.1597 ], [ -73.306846, 41.160054 ], [ -73.305741, 41.160534 ], [ -73.304731, 41.159169 ], [ -73.305701, 41.158767 ], [ -73.306681, 41.158365 ], [ -73.30669, 41.158355 ], [ -73.306699, 41.158367 ], [ -73.307344, 41.159265 ], [ -73.307356, 41.159282 ], [ -73.308345, 41.158849 ], [ -73.309003, 41.158184 ], [ -73.309406, 41.158118 ], [ -73.309488, 41.158222 ], [ -73.309514, 41.158255 ], [ -73.30958, 41.158395 ], [ -73.309607, 41.158521 ], [ -73.309679, 41.158498 ], [ -73.310094, 41.159039 ], [ -73.31032, 41.159327 ], [ -73.310363, 41.159382 ], [ -73.310413, 41.159413 ], [ -73.310442, 41.159435 ], [ -73.310462, 41.15945 ], [ -73.310476, 41.159471 ], [ -73.310683, 41.159784 ], [ -73.310336, 41.159947 ], [ -73.310318, 41.15996 ], [ -73.310314, 41.159957 ], [ -73.310288, 41.159935 ], [ -73.310237, 41.159892 ], [ -73.310161, 41.159827 ], [ -73.310032, 41.159717 ], [ -73.309905, 41.15961 ], [ -73.309566, 41.159632 ], [ -73.309443, 41.159458 ], [ -73.309306, 41.159281 ], [ -73.309164, 41.159346 ], [ -73.308735, 41.159534 ], [ -73.308547, 41.159616 ], [ -73.308519, 41.159626 ], [ -73.308168, 41.159753 ], [ -73.308878, 41.160773 ], [ -73.308938, 41.160747 ], [ -73.309503, 41.161501 ], [ -73.309102, 41.161669 ], [ -73.30854, 41.160896 ] ] ]}}]}
ltmid: '29'
region: fairfield
size: '16'
image: /dist/assets/27_PUTNAM_AND_RUDKIN_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/putnam-rudkin-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.165193,-73.306698&q=299+Queens+Grant+Drive++Fairfield,+CT,+06824,+United+States&z=12
categories:
  - category: hiking
---
Putnam preserve is 8 acres generously donated by Mrs. Frances Snyder in 1992. This trailed flat wooded preserve is marked with stone walls containing mostly wetlands with Sasco Creek nearby. It is mostly used by the Fairfield Bridle Trails Association, as it provides access to Rudkin Preserve trails which are for equestrian use only. The Putnam Preserve is access by a Pent Road bordered by stone walls and which used to be used by onion farmers to take their crops to Southport Harbor.

## Directions & Parking:

Access to this preserve is off of Queens Grant Drive off of Silver Spring Road. Park on Queens Grant Drive and walk up the Pent Road which is the walking path between double stone walls.
