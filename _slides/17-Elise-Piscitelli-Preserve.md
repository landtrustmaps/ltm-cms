---
title: Elise Piscitelli Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.305578, 41.213249 ], [ -73.306333, 41.21289 ], [ -73.306335250191509, 41.212890051728543 ], [ -73.306335249917524, 41.212890042412873 ], [ -73.306066, 41.212884 ], [ -73.306132, 41.212852 ], [ -73.306847, 41.212512 ], [ -73.306903, 41.212578 ], [ -73.306989, 41.212673 ], [ -73.307094, 41.212799 ], [ -73.30715, 41.21287 ], [ -73.30718, 41.212909 ], [ -73.307173275751126, 41.212908849096749 ], [ -73.307173836303477, 41.212909329570195 ], [ -73.307203, 41.21291 ], [ -73.307243, 41.212958 ], [ -73.307378, 41.21312 ], [ -73.307409, 41.213164 ], [ -73.307431, 41.213211 ], [ -73.307444, 41.213259 ], [ -73.307448, 41.213309 ], [ -73.307442, 41.213358 ], [ -73.307427, 41.213406 ], [ -73.307403, 41.213452 ], [ -73.307371, 41.213495 ], [ -73.307331, 41.213534 ], [ -73.307284, 41.213568 ], [ -73.30723, 41.213597 ], [ -73.306286, 41.214052 ], [ -73.305578, 41.213249 ] ] ]}}]}
ltmid: 16
region: easton
size: '4'
image: /dist/assets/9_ELISE_PISCITELLI_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/elise-piscitelli-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.213958,-73.306666&q=150+Twin+Lanes+Road+Fairfield,+CT,+06824,+United+States&z=12
categories: []
---
The Piscitelli family owned a 3.5-acre lot on Twin Lanes Road, in Easton, for many years. It is located in an older neighborhood and is clearly an old farm field that is starting to return to its natural state: woods. Elisa Piscitelli generously donated this parcel in 2006. By clearing some of the land and thus returning this open space to fields, we improved the habitat for birds and other wildlife.


# Directions & Parking

From North Street: Turn on Twin Lanes and park roadside.
