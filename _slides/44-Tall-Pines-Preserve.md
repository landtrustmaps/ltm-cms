---
title: Tall Pines Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.345481,41.200598],[-73.345388,41.200467],[-73.345181,41.200176],[-73.34491,41.199831],[-73.344846,41.199749],[-73.344954,41.199712],[-73.344896,41.199641],[-73.343997,41.198542],[-73.343516,41.198737],[-73.343601,41.198853],[-73.343412,41.198938],[-73.343062,41.198447],[-73.342727,41.198564],[-73.342105,41.197734],[-73.342172,41.197709],[-73.342244,41.197683],[-73.34234,41.197648],[-73.342409,41.197622],[-73.342515,41.197585],[-73.342593,41.197558],[-73.342663,41.197536],[-73.342678,41.197532],[-73.342711,41.197524],[-73.342765,41.197512],[-73.342812,41.197506],[-73.342828,41.197503],[-73.342843,41.197501],[-73.342858,41.1975],[-73.342873,41.197499],[-73.342888,41.197498],[-73.342903,41.197498],[-73.342918,41.197498],[-73.342933,41.197498],[-73.342948,41.197499],[-73.342963,41.1975],[-73.342979,41.197502],[-73.342981,41.197502],[-73.343005,41.197506],[-73.343036,41.197513],[-73.343882,41.19855],[-73.344023,41.198503],[-73.344263,41.198793],[-73.344999,41.199685],[-73.345136,41.199634],[-73.345442,41.200015],[-73.346446,41.199595],[-73.347055,41.199424],[-73.34706,41.199432],[-73.347099,41.1995],[-73.347129,41.19955],[-73.346537,41.199719],[-73.345529,41.200143],[-73.34586,41.200575],[-73.3464,41.201278],[-73.346302,41.201375],[-73.346101,41.201468],[-73.345481,41.200598]]]}},{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.340946,41.198014],[-73.341004,41.198032],[-73.341117,41.198021],[-73.342671,41.197366],[-73.342807,41.197361],[-73.342955,41.197411],[-73.342784,41.197156],[-73.342846,41.197135],[-73.342805,41.197089],[-73.345894,41.195814],[-73.345549,41.195443],[-73.342396,41.196727],[-73.342466,41.196802],[-73.340641,41.197576],[-73.340946,41.198014]]]}}]}
ltmid: '14'
region: weston
size: '14'
image: /dist/assets/23_TALL_PINES_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/tall-pines-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.197681,-73.346262&q=43+Tall+Pines+Drive+Weston,+CT,+06883,+United+States&z=12
categories:
  - category: hiking
---
When combined with Taylor Woods, this Preserve provide a varied and interesting one-hour hike.  The trail starts from Twin Walls Lane across from the restored twin stonewalls that makred an early Weston roadway.  Follow the train along some wetlands adn over several brooks until it joins a grassy path and dotted with berries, honesuckle, ferns and sassafras that leads into the Taylor Woods Preserve.  The Preserve is names for a spectacular grove of white pines that once occupied the area.


# Historical Note:

Early Weston was notorious for bad roads.  One reason: the locals refused to be taxed for construction and maintenance.  In 1791, the town voted that all able-bodied Norfield men should each work one full day to help repair Fanton Hill Road.

# Directions & Parking

Directions & Parking: North on Lyons Plain Road; right on Fanton Hill Road, right on Tall Pines Road, right on Twin Walls Lane.  Park along the road.
