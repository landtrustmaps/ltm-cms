---
title: Poindexter Nature Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.292229, 41.319673 ], [ -73.292196, 41.319853 ], [ -73.291547, 41.319944 ], [ -73.291059, 41.319985 ], [ -73.290686, 41.320044 ], [ -73.290221, 41.320105 ], [ -73.289567, 41.320191 ], [ -73.289339, 41.320221 ], [ -73.289096, 41.320254 ], [ -73.28672, 41.320573 ], [ -73.286442, 41.3206 ], [ -73.285758, 41.320675 ], [ -73.285511, 41.32071 ], [ -73.285231, 41.320736 ], [ -73.285161, 41.320744 ], [ -73.284915, 41.32077 ], [ -73.284884, 41.32071 ], [ -73.284861, 41.320645 ], [ -73.284836, 41.320593 ], [ -73.284783, 41.320491 ], [ -73.284756, 41.320426 ], [ -73.28475, 41.320408 ], [ -73.284709, 41.320326 ], [ -73.284651, 41.320226 ], [ -73.284616, 41.320157 ], [ -73.284543, 41.320043 ], [ -73.284374, 41.319738 ], [ -73.284342, 41.31969 ], [ -73.284289, 41.3196 ], [ -73.284113, 41.3193 ], [ -73.283891, 41.318853 ], [ -73.283813, 41.318737 ], [ -73.283826, 41.318732 ], [ -73.283582, 41.318275 ], [ -73.283281, 41.317744 ], [ -73.283059, 41.317394 ], [ -73.283278, 41.317307 ], [ -73.284609, 41.316811 ], [ -73.284866, 41.316716 ], [ -73.285238, 41.316587 ], [ -73.285608, 41.316502 ], [ -73.28637, 41.317808 ], [ -73.288618, 41.31826 ], [ -73.289558, 41.320039 ], [ -73.290674, 41.319882 ], [ -73.290641, 41.319613 ], [ -73.290746, 41.319598 ], [ -73.29227, 41.31938 ], [ -73.292229, 41.319673 ] ] ]}}]}
ltmid: '22'
region: easton
size: '40'
image: /dist/assets/3_POINDEXTER_NATURE_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/poindexter-nature-preserve/'
googlemaps: >-
  https://www.google.com/maps/place/400+Judd+Rd,+Easton,+CT+06612/@41.3161769,-73.2926418,19z/data=!3m1!4b1!4m5!3m4!1s0x89e807f117992c7f:0x82144d186c3d8ee3!8m2!3d41.3161769!4d-73.2920946
categories:
  - category: hiking
---
One of our nicest and most interesting preserves, the entrance from Judd Road opens to an old field which is adorned with beautiful dogwood trees. The trail leads from this field through a mature white pine forest and across a stream to an old fieldstone dam. Easton Scout, Will Hermann, built a bridge over wetlands in 2006 which allows visitors to keep their socks dry! While you enjoy a wonderful 45 minute to one hour hike through a meadow and pine and hardwood forest, look for a wide range of stonewalls from single to double width and then explore a pond created by the hand-laid stone dam.


# Directions & Parking

Preserve entrance is located on Judd Road between Maple Rd & Knapp Street. Parking is road side and there is room for one car on each side of the road by the entrance.
