---
title: LeGallienne Bird Sanctuary
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.394268,41.216564],[-73.393616,41.216794],[-73.392986,41.216917],[-73.392868,41.217541],[-73.392965,41.217879],[-73.392396,41.217987],[-73.391956,41.218155],[-73.391754,41.217998],[-73.391461,41.217439],[-73.39114,41.217636],[-73.390542,41.217876],[-73.390417,41.217673],[-73.390291,41.217402],[-73.390389,41.216941],[-73.390598,41.216478],[-73.390662,41.216294],[-73.390751,41.216108],[-73.390958,41.215995],[-73.391201,41.216014],[-73.391959,41.216602],[-73.392936,41.2164],[-73.393033,41.215849],[-73.393155,41.214863],[-73.393523,41.214957],[-73.39376,41.215403],[-73.394084,41.216136],[-73.394268,41.216564]]]}}]}
ltmid: '2'
region: weston
size: '12.5'
image: /dist/assets/12_LEGALLIENNE_BIRD_SANCTUARY.jpg
related: 'https://www.aspetucklandtrust.org/legallienne-bird-sanctuary/'
googlemaps: >-
  https://www.google.com/maps?sll=41.217908,-73.392875&q=1+September+Lane+Weston,+CT,+06883,+United+States&z=12
categories:
  - category: birding
---
We are currently restoring this property as a bird sanctuary.  Don't be alarmed by the trees you see cut. We are creating "early successional habitat" or young forest to attract more scrub shrub birds like the Chestnut Sided Warbler, Eastern Towhee and Common Yellow Throat, the fastest declining bird species in CT.   The trails on the property, occasionally steep, are about a mile long as it crisscrosses the hillside through sparse woodlands, over a small meadow, by several unusual rock outcroppings, and, of special note, the large boulder Ms. LeGallienne used to practice for her Broadway role as Peter Pan. Occasional rock piles on the hillside mark the former sites of barns or houses and indicating the probability of subsistence farming.



# Historical Note:

Ms. LeGallienne wrote that her home was more than 200 years old when she bought it in 1926.  Her move to this haven . . . "helped me sustain my physical and spiritual energy.....that there is not a day when one is not overwhelmed by the beauty of the world."



You can learn more about Eva LeGallienne by clicking [here](http://www.imdb.com/name/nm0499170/) and [here](http://en.wikipedia.org/wiki/Eva_Le_Gallienne).
