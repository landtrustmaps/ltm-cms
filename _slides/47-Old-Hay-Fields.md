---
title: Old Hay Fields
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.2976,41.154634],[-73.29635,41.154803],[-73.296067,41.154154],[-73.297175,41.15395],[-73.2976,41.154634]]]}},{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.299578,41.155176],[-73.298651,41.155431],[-73.298285,41.15471],[-73.299179,41.154487],[-73.299578,41.155176]]]}}]}
ltmid: '31'
region: fairfield
size: '2.5'
image: /dist/assets/35_OLD_HAY_FIELDS.jpg
related: 'https://www.aspetucklandtrust.org/old-hay-fields/'
googlemaps: >-
  https://www.google.com/maps?sll=41.154615,-73.298164&q=1000+Cedar+Road+Fairfield,+CT,+06890,+United+States&z=12
categories:
  - category: hiking
---
Beautiful old haying fields at the corner of Morehouse & Cedar bordered with crabapple trees and stone walls. A reminder of the areas farming days in this lovely area of Southport.

This land was generously donated by Ruth Berlin with a second parcel purchased with the help of neighbors to protect the beauty of the fields. These fields also provide access to a bridle trail that is enjoyed by local riders. The Hay field located on Cedar Road is connected to a small Town of Fairfield Open Space.


# Directions & Parking

Preserves are located at the corner of Cedar Lane & Morehouse Rd. Park roadside on Cedar Lane.<br><br><br><br>
