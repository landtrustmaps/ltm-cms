---
title: Hilla Von Rebay Arboretum
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.32221,41.129579],[-73.321583,41.1298],[-73.321169,41.129756],[-73.320587,41.130173],[-73.320958,41.130481],[-73.320954,41.131119],[-73.322521,41.130516],[-73.32221,41.129579]]]}}]}
ltmid: '40'
region: wesport
size: '9'
image: /dist/assets/43_HILLA_VON_REBAY_ARBORETUM.jpg
related: 'https://www.aspetucklandtrust.org/hilla-von-rebay-arboretum/'
googlemaps: >-
  https://www.google.com/maps?sll=41.130176,-73.322618&q=83+Morningside+Drive+South+Westport,+CT,+06880,+United+States&z=12
categories:
  - category: hiking
---
Former estate of Hilla von Rebay, an artist and co-founder of the Guggenheim Museum. Here you will find wide open space of former lawns and gardens, specimen trees and wooded wetlands. The Von Rebay house and barn still stand abutting the north side of the preserve.


# Directions & Parking

From Greens Farms Road, make a left onto Clapboard Hill; take a left turn onto Morningside Drive; drive about 0.2 of a mile and the preserve entrance will be on the right, just past house #83. Park on the left side of the road, youll find the preserve across the street.
