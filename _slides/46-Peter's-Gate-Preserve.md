---
title: Peter's Gate Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.29885,41.140647],[-73.299912,41.141887],[-73.301001,41.141602],[-73.301077,41.141653],[-73.301183,41.141791],[-73.301312,41.141765],[-73.301676,41.142169],[-73.30201,41.142065],[-73.301472,41.141208],[-73.300978,41.141402],[-73.300495,41.141424],[-73.299984,41.141112],[-73.300052,41.14067],[-73.299606,41.140317],[-73.29885,41.140647]]]}}]}
ltmid: '39'
region: westport
size: '6'
image: /dist/assets/44_PETERS_GATE_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/peters-gate-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.14288,-73.300828&q=North+Sasco+Cmns+Westport,+CT,+06880,+United+States&z=12
---
Peter’s Gate is mostly wetlands with rich waterfowl habitat at Bulkley Pond which ultimately flows into Fairfield’s Sasco Creek Marsh open space and eventually Long Island Sound. This is a wildlife only preserve.

## Directions & Parking: 

Bulkley Avenue North; drive 0.3 of a mile and turn right onto North Sasco Common. Park on the north side of the street.
