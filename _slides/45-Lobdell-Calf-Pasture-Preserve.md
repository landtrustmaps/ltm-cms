---
title: Lobdell Calf Pasture Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.289107, 41.191641 ], [ -73.289328, 41.191559 ], [ -73.289405, 41.191504 ], [ -73.28942, 41.191493 ], [ -73.28955, 41.191402 ], [ -73.289569, 41.191388 ], [ -73.289709, 41.191287 ], [ -73.289892, 41.191154 ], [ -73.289933, 41.191132 ], [ -73.289942, 41.191074 ], [ -73.28996, 41.191085 ], [ -73.290336, 41.191501 ], [ -73.290347, 41.191513 ], [ -73.291102, 41.192356 ], [ -73.29111, 41.192364 ], [ -73.290783, 41.192455 ], [ -73.290014, 41.192661 ], [ -73.289974, 41.192672 ], [ -73.28997, 41.192668 ], [ -73.289107, 41.191641 ] ] ]}}]}
ltmid: '24'
region: fairfield
size: '14'
image: /dist/assets/29_LOBDELL_CALF_PASTURE_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/lobdell-calf-pasture-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.190283,-73.290041&q=220+High+Point+Lane+Fairfield,+CT,+06824,+United+States&z=12
---
Connected to the Hillman Preserve, the Lobdell Calf Pasture is a great example of old farmland returning to forest with stone walls and a seasonal pond. It is part of a 6-lot subdivision of the Lobdell property which used to be farm. Access to the preserve is from the Hillman preserve. The preserve is not trailed. 

## Directions & Parking:

Lobdell Calf Pasture Preserve is located at the end of High Point Lane off Hillside Road. Park roadside at the cul-de-sac.
