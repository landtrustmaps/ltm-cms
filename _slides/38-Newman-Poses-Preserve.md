---
title: Newman Poses Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.347862,41.1879],[-73.347098,41.188155],[-73.345899,41.186127],[-73.344766,41.186515],[-73.343601,41.18696],[-73.343633,41.187114],[-73.343336,41.187199],[-73.343191,41.187055],[-73.343397,41.186877],[-73.344793,41.18639],[-73.34449,41.185325],[-73.34341,41.185167],[-73.34354,41.184807],[-73.343807,41.183689],[-73.344396,41.183842],[-73.345122,41.18364],[-73.345627,41.18404],[-73.34608,41.184775],[-73.347897,41.184426],[-73.347092,41.183416],[-73.348621,41.182874],[-73.34981,41.184332],[-73.348562,41.185371],[-73.348116,41.186358],[-73.347718,41.187182],[-73.347862,41.1879]]]}}]}
ltmid: '38'
region: westport
size: '39'
image: /dist/assets/46_NEWMAN_POSES_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/newman-poses-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.185751,-73.34579&q=Newman-Poses+Nature+Preserve+Westport,+CT,+USA&z=12
categories:
  - category: hiking
---
Say hello to a flock of wild turkeys while you enjoy the diverse habitats of this preserve. Here you will find well-established trails through woodland, wetland, open fields, stands of old white pine, and a trail beside the Aspetuck River. This nature preserve is the only public memorial approved by the family of the late Paul Newman as a way to honor the actor and philanthropist.

In spring 2018 with help from interns from Staples High School, Aspetuck Land Trust is improving the entrance area.  Gault Energy is donating topsoil and gravel, the Town of Westport is lending their public works crew, Barts Tree Service is removing invasive trees, and Oliver's Nursery is donating native plants. Local woodworker Dan Magner from Easton is designing a new preserve sign.   The Land Trust worked closely with the town of Westport and the Newman family to ensure that this land will remain open space for the public and not be developed. The Land Trust made improvements to the property for public access (driveway and parking lot, trails, bridges, signage) and manages the nature preserve for the town.

Mr. Newman lived near the property and donated a large portion of the land to the town. The parcel also includes land sold to the town by Lillian Poses, a neighbor and friend of Mr. Newman's who worked on the New Deal in Franklin D. Roosevelt's administration and who was one of the first women to graduate from NYU Law School


# Directions & Parking

The preserve can be accessed off Bayberry Lane (north of Easton Road/Rte 136). The entrance to the preserve is located between 307 and 313 Bayberry Lane. There is a small 3 car parking lot. If the lot is full, you can park at the turn off at the intersection of Easton Road/Route 136 and Bayberry Lane and walk 500 feet north to the entrance.
