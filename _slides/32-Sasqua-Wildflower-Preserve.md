---
title: Sasqua Wildflower Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.291249,41.133145],[-73.290773,41.133097],[-73.290978,41.132403],[-73.29192,41.132572],[-73.291249,41.133145]]]}}]}
ltmid: '30'
region: fairfield
size: '0.3'
image: /dist/assets/32_SASQUA_WILDFLOWER_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/sasqua-wildflower-preserve/ '
googlemaps: >-
  https://www.google.com/maps?sll=41.133185,-73.290816&q=360+Westway+Road+Fairfield,+CT,+06890,+United+States&z=12
categories:
  - category: hiking
---
Our smallest preserve located across from the historic Pequot Library in Southport. Mowed walking trails make this a lovely spot to observe wildflowers in season. Donated in 1981 by Roswell Barrett who owned the home that is located at the back of the preserve.


# Directions & Parking

Westway Road; park roadside by split rail fence, across from the Pequot Library entrance.
