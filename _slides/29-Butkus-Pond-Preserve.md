---
title: Butkus Pond Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.257636,41.174111],[-73.25769,41.174576],[-73.257549,41.17488],[-73.257017,41.175104],[-73.256754,41.174907],[-73.256637,41.173784],[-73.256447,41.173364],[-73.257096,41.17326],[-73.257253,41.173584],[-73.257516,41.173747],[-73.257636,41.174111]]]}}]}
ltmid: '27'
region: fairfield
size: '4'
image: /dist/assets/33_BUTKUS_POND_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/butkus-pond-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.173491,-73.257507&q=100+Penny+Ln+Fairfield,+CT,+06824,+United+States&z=12
categories:
  - category: hiking
  - category: fishing
  - category: children
---
This hidden preserve in a quiet neighborhood contains a large "catch & release" pond with fathead minnows, bluegills, and some bass - perfect for fishing with children.  The pond contains duckweed, with wild raisin, cowslip, swamp maple, and royal fern around the edge.

Look for the eastern painted turtles sunning on the rocks and water striders skimming on the surface!<br>


# Directions & Parking

Located at the end of Penny Lane cul-de-sac, off North Benson Road (Rt. 135) just south of Stillson Rd. Alterative entrance and parking is available on Centerbrook Rd.
