---
title: Jennings Wood Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.35684, 41.245576 ], [ -73.35693, 41.245545 ], [ -73.358272, 41.245097 ], [ -73.357939, 41.244603 ], [ -73.358279, 41.244496 ], [ -73.358456, 41.244728 ], [ -73.358933, 41.244537 ], [ -73.358988, 41.244607 ], [ -73.359621, 41.245422 ], [ -73.359604, 41.245441 ], [ -73.359601, 41.245445 ], [ -73.359587, 41.245462 ], [ -73.359586, 41.245464 ], [ -73.359568, 41.245488 ], [ -73.359565, 41.245492 ], [ -73.359554, 41.24551 ], [ -73.359553, 41.245512 ], [ -73.359545, 41.245525 ], [ -73.359543, 41.245528 ], [ -73.359528, 41.245558 ], [ -73.359527, 41.245559 ], [ -73.359477, 41.245663 ], [ -73.359418, 41.245786 ], [ -73.359417, 41.245789 ], [ -73.359395, 41.245842 ], [ -73.359346, 41.245967 ], [ -73.359345, 41.24597 ], [ -73.359323, 41.246037 ], [ -73.359302, 41.246094 ], [ -73.359271, 41.246172 ], [ -73.359245, 41.246236 ], [ -73.359214, 41.246316 ], [ -73.359214, 41.246318 ], [ -73.359195, 41.24637 ], [ -73.35916, 41.246456 ], [ -73.35916, 41.246457 ], [ -73.359133, 41.246527 ], [ -73.359108, 41.2466 ], [ -73.359088, 41.246655 ], [ -73.359054, 41.246741 ], [ -73.359053, 41.246743 ], [ -73.359033, 41.246802 ], [ -73.359027, 41.246818 ], [ -73.359003, 41.246877 ], [ -73.358976, 41.246941 ], [ -73.358955, 41.246992 ], [ -73.358954, 41.246994 ], [ -73.358928, 41.247067 ], [ -73.3589, 41.24715 ], [ -73.358876, 41.247226 ], [ -73.358864, 41.247265 ], [ -73.358863, 41.247268 ], [ -73.358862, 41.24727 ], [ -73.35886, 41.247286 ], [ -73.358853, 41.247322 ], [ -73.358847, 41.247368 ], [ -73.358836, 41.247439 ], [ -73.358828, 41.247483 ], [ -73.358828, 41.247485 ], [ -73.358823, 41.24752 ], [ -73.358823, 41.247523 ], [ -73.358821, 41.247549 ], [ -73.358821, 41.247552 ], [ -73.358821, 41.247576 ], [ -73.358821, 41.247578 ], [ -73.358822, 41.24762 ], [ -73.358822, 41.247623 ], [ -73.358826, 41.247683 ], [ -73.358828, 41.247741 ], [ -73.358828, 41.247743 ], [ -73.358833, 41.247807 ], [ -73.358834, 41.247852 ], [ -73.358835, 41.247898 ], [ -73.358835, 41.247901 ], [ -73.358839, 41.247965 ], [ -73.358841, 41.248028 ], [ -73.358842, 41.248045 ], [ -73.358854, 41.248381 ], [ -73.35684, 41.245576 ] ] ] }}]}
ltmid: '6'
region: weston
size: '11'
image: /dist/assets/16_JENNINGS_WOODS_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/jennings-woods-preserve'
categories:
  - category: hiking
---
A glimpse of how much of Weston may have looked to the first English settlers 300 years ago. The red trail is a 20-minute walk around and over marshy wetlands, and through unusually dense woodlands. The trail reveals a diverse mix of plant life, including a pervasive undergrowth of laurel. A singular knoll covered with specimen beech trees marks the trails end. The white trail connects to the extensive Aquarian trail system along the Saugatuck Reservoir.

# Historical Note:

Weston was not home to any of the five nomadic clans that made up the Paugausett tribe; however Weston was a choice area for their hunting and gathering forays with its blackberries, currents, and magnificent stands of chestnuts and walnuts.

# Directions & Parking:

Newtown Turnpike (Rt 53) north to Godfrey Road; right on Godfrey; right on Catbrier; left on Pepperbush. Preserve entrance marked by sign on right. Park along Pepperbush.
