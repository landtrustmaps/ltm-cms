---
title: Randall's Farm Nature Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.28984199,41.26201574],[-73.28986066,41.26200486],[-73.29042094,41.26182173],[-73.2905596,41.26176658],[-73.29059099,41.26176109],[-73.29130737,41.26153075],[-73.2915013,41.26146463],[-73.2915431,41.26152145],[-73.29189691,41.26193137],[-73.29240667,41.26159737],[-73.29250212,41.26154271],[-73.29277318,41.26136754],[-73.29337187,41.2618707],[-73.2935141,41.26179327],[-73.29362195,41.26171918],[-73.29375577,41.26162727],[-73.29425405,41.26127526],[-73.29433104,41.26121129],[-73.2944122,41.2613231],[-73.29469607,41.26165614],[-73.29487613,41.2618655],[-73.29496511,41.26195973],[-73.29510041,41.26212612],[-73.29525645,41.26231766],[-73.29533352,41.26241225],[-73.29537863,41.26246311],[-73.29547987,41.26257724],[-73.29549385,41.262595],[-73.29570609,41.26286463],[-73.29576386,41.26293802],[-73.29588508,41.26308299],[-73.29596582,41.26319007],[-73.29604742,41.26328289],[-73.29621676,41.26349725],[-73.29644057,41.26377874],[-73.29662259,41.26400851],[-73.2966428,41.26402681],[-73.29672704,41.26413902],[-73.29561854,41.26465569],[-73.29542488,41.26474371],[-73.295271,41.26482304],[-73.2952234,41.26484026],[-73.29507049,41.26491919],[-73.29481791,41.2650377],[-73.2946033,41.26513838],[-73.29413621,41.26459401],[-73.29245587,41.26535704],[-73.29228561,41.26515583],[-73.29198131,41.26479621],[-73.29188334,41.26468043],[-73.29249716,41.26438473],[-73.29251169,41.2643776],[-73.29226736,41.26408874],[-73.29292411,41.26377234],[-73.29225776,41.26302716],[-73.29104187,41.26366545],[-73.29077385,41.26333823],[-73.29061341,41.26321133],[-73.29054853,41.26316001],[-73.2905242,41.26314062],[-73.2903963,41.26298903],[-73.29022488,41.26271961],[-73.29016553,41.26264947],[-73.28966771,41.26206103],[-73.28969931,41.26205485],[-73.28984199,41.26201574]]]}}]}
ltmid: '23'
region: easton
size: '48'
image: /dist/assets/10_RANDALLS_FARM_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/randalls-farm-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.2636,-73.290816&q=700+Sport+Hill+Road+Easton,+CT,+06612,+United+States&z=12
---
A former dairy farm operated by the Randall family, Randall's Farm preserve is a scenic 34-acre expanse of meadows, fields, forested wetlands and hardwood forest. There are also small ponds and snapping turtles. Generously donated by Mrs. Henry B. DuPont III who acquired the property in 1983, the preserve officially opened in June 2012.

Enjoy this beautiful preserve with open meadows, stone walls, a pond and groomed trails through high grassy wildflower meadows. It is one of the last great old farm fields and areas few remaining historic long lots.

# Historical Note:  

In colonial times, a long lot system of land division by local government leaders deeded large parcels of land at a very attractive price to earlier settlers. The goal was to encourage settlers to move inland from increasingly populous coastal Fairfield and clear the land for agriculture in the more remote, unsettled interior. The land division of 1671 created two special sections in northern Fairfield that would eventually become Weston and Easton.

# Directions & Parking:

Preserve is located one mile north of Silverman's Farm on Sport Hill Rd (Rt. 59) in Easton. Small parking lot is located in preserve off Sport Hill Road. For additional parking, take Adams Rd to Kellers Farm Rd and park at the cul-de-sac.
