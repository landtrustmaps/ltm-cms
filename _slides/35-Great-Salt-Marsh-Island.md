---
title: Great Salt Marsh Island
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.231519,41.147869],[-73.231047,41.146201],[-73.230306,41.145492],[-73.23103,41.144843],[-73.232378,41.144631],[-73.235524,41.144278],[-73.236181,41.145293],[-73.236295,41.146544],[-73.23448,41.14757],[-73.233462,41.148238],[-73.231519,41.147869]]]}}]}
ltmid: '35'
region: bridgeport
size: '14'
image: /dist/assets/38_GREAT_SALT_MARSH_ISLAND.jpg
related: 'https://www.aspetucklandtrust.org/great-salt-marsh-island/'
googlemaps: >-
  https://www.google.com/maps?sll=41.148614,-73.237406&q=417+Riverside+Drive+Fairfield,+CT,+06824,+United+States&z=12
categories:
  - category: hiking
  - category: fishing
---
In 2004 the possibility of the development of Great Salt Marsh Island was brought to the attention of the Aspetuck Land Trust by the Ash Creek Conservancy.  Since Great Salt Marsh Island was bisected by the Fairfield-Bridgeport town line, the Land Trust approached the Town of Fairfield and agreed to split the cost and the land with Fairfield, which was also interested in perserving this open space.  Aspetuck Land Trust took ownership of the 7 acres located in Bridgeport and the Town bought the 7 acres located in Fairfield.  In September of 2005, Great Salt Marsh Island was purchased and this important environmental and cultural resource was preserved forever.

The Ash Creek estuary is a lively place indeed! On any given day you are bound to be treated to a glimpse of its many inhabitants. . .if you look carefully. Low tide brings out the best.  As you walk along the edge of the marsh area you will find an army of fiddier crabs scavenging for food which includes both plant and animal remains. Also look for the broken shells of mussels which live with barnacles on the rocky jetty alongside the marina and clams and oysters.  The salt marshes draw a crowd of fish, both big and small. If you happen to walk the trail as the tide is rising, look for minnows, mummichugs and killifish. These fish and the marsh grasses are food for a variety of larger fish as well. Several of the popular recreational and commercial species, including winter flounder and bluefish, breed and spend their early lives in these protective food-filled marshes.

This island marsh is the only tidal wetland remaining in the area that has not been filled, diked for flood relief or ditched for mosquito control purposes it is essentially in its natural state. The Town of Fairfield and the Aspetuck Land Trust are proud to have preserved this important part or the areas history.


# Visiting Great Salt Marsh Island

There is no direct access to the Island. Due to its environmentally sensitive nature this is probably a good thing.  The Island is easily viewable from both Fairfield and Bridgeport.  The best location in Bridgeport is from the sand spit located at the mouth of the creek off of Gilman Street. From that point viewers can walk down Gilman Street to see the entire island. At Balmforth street, looking west towards Fairfield, at low tide you can see the remains of the foundation for the old corduroy road that was constructed in colonial times (see Ash Creek History below).  The best location in Fairfield is from the Ash Creek Open Space located at the end of Turney Road. Drive past the gate house, take an immediate left, drive through the boaters parking area to the Open Space parking.  The island is visible from this area, but a walk out the narrow peninsula that divides Ash Creek from South Benson Marina provides better views.

# Directions & Parking

Fairfield Marina off Turney Rd<br>Fairfield, CT 06824
