---
title: Singing Oaks
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.371176, 41.247963 ], [ -73.370333, 41.248459 ], [ -73.370199, 41.248248 ], [ -73.368312, 41.24911 ], [ -73.368876, 41.248116 ], [ -73.369891, 41.247785 ], [ -73.369589, 41.247324 ], [ -73.369218, 41.246759 ], [ -73.369803, 41.246519 ], [ -73.370076, 41.246407 ], [ -73.37211, 41.245649 ], [ -73.37277, 41.245403 ], [ -73.373144, 41.245894 ], [ -73.373094, 41.246084 ], [ -73.373984, 41.247558 ], [ -73.37408, 41.247667 ], [ -73.373951, 41.247714 ], [ -73.373668, 41.247607 ], [ -73.373112, 41.24634 ], [ -73.371884, 41.246935 ], [ -73.372693, 41.24805 ], [ -73.372195, 41.248413 ], [ -73.372035, 41.248525 ], [ -73.371176, 41.247963 ] ] ]}}]}
ltmid: '90'
region: weston
size: '18'
image: /dist/assets/15_SINGING_OAKS_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/singing-oaks-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.247714,-73.374207&q=100+Equestrian+Drive+Weston,+CT,+06883,+United+States&z=12
categories:
  - category: hiking
---
This preserve is perfect for the ardent hiker, and well worth it. The preserve is a powerful display of natures raw beauty -- dramatic rock outcroppings, cliffs, ridges, meandering streams and boulder fields connected by a two-mile trail that is narrow, rocky and steep. Vernal pools and deep woods add to the lure. The chance of spotting wildlife is good.

## Historical Note:  

In the late 1600s, the area was an overnight stopping point for the Paugausett Indians making seasonal treks between their upland and coastal camps. In more recent times, the flatter environs were the site of a well-know day camp. 

## Directions & Parking: 

Newtown Turnpike (Route 53) north; right on Singing Oaks Drive; right on Equestrian Drive to three- car parking area at end of the Lane. Trail entrance marked by a wood sign about 50 feet east of the parking area.
