---
title: Island Pond Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.307063, 41.312647 ], [ -73.307246, 41.312726 ], [ -73.307241, 41.312926 ], [ -73.307207, 41.313186 ], [ -73.307143, 41.313358 ], [ -73.307026, 41.313598 ], [ -73.306805, 41.313842 ], [ -73.306453, 41.31399 ], [ -73.305993, 41.314033 ], [ -73.30569, 41.313707 ], [ -73.305529, 41.3134 ], [ -73.305361, 41.312796 ], [ -73.305516, 41.312789 ], [ -73.306057, 41.312783 ], [ -73.306363, 41.312792 ], [ -73.306667, 41.31275 ], [ -73.307063, 41.312647 ] ] ]}}]}
ltmid: '20'
region: easton
size: '4'
image: /dist/assets/2_ISLAND_POND_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/island-pond-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.308249,-73.301484&q=220+Maple+Road+Easton,+CT,+06612,+United+States&z=12
categories:
  - category: fishing
  - category: hiking
---
Island Pond Preserve is part of the 128-acre Paine Open Space owned by the town of Easton. Paine Open Space has an extensive trail system that is excellent for year-round hiking and snowshoeing or cross country skiing in winter.

Follow the trail past several small ponds to Island Pond where you can watch nature from the sheltered island. A bridge (rebuilt in 1987) is a nice place to rest and picnic. Enjoy the wonderful pond lilies. The former Paine residence is located to the south of the pond. This pond, as well as, Pond View was built by Ralph Paine. He pumped water from this pond to the other seven ponds that are above this one. See the remains of an old cement block pump house on the east side of the pond. Bring your fishing pole for bass and sunfish.


# Directions & Parking

Follow Stepney Road/ Rt 59 to a left turn onto Judd Road; turn left. Right on Maple Road (shortly past April Drive) and go 1 mile. Turn right into the main entrance (easy to miss because the sign is small).
