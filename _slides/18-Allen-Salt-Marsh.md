---
title: Allen Salt Marsh
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.342977,41.119723],[-73.343293,41.11966],[-73.343574,41.120384],[-73.342548,41.120969],[-73.342335,41.12076],[-73.341258,41.121301],[-73.34062,41.120988],[-73.34071,41.120771],[-73.341177,41.120842],[-73.341994,41.120573],[-73.342006,41.120232],[-73.342096,41.119932],[-73.342075,41.119258],[-73.342318,41.118962],[-73.34261,41.11888],[-73.342734,41.119287],[-73.342977,41.119723]]]}}]}
ltmid: '43'
region: westport
size: '7'
image: /dist/assets/42_ALLEN_SALT_MARSH.jpg
related: 'https://www.aspetucklandtrust.org/allen-salt-marsh/'
googlemaps: >-
  https://www.google.com/maps?sll=41.120876,-73.342903&q=30+Grove+Point+Westport,+CT,+06880,+United+States&z=12
categories:
  - category: birding
---
This beautiful salt marsh preserve on the western side of the Mill Pond is non-trailed but offers access for viewing the wildlife and scenery of the spectacular Mill Pond. Donated by the Northrop family and named for Captain Walter D. Allen, the marsh was preserved with the help of The Nature Conservancy. In 1989, Edward Hicks added to the preserve with a donation of over ½ acre of land to help create a beautiful viewing area.

Marshes are one of the most biologically productive types of wetland. Seasonal flooding continually adds nutrient-rich water and sediments to marshes. These nutrients nourish plants which, in turn, attract other wildlife. It is estimated that in terms of plant production, marshes are three times as productive as agricultural land and four times as productive as lakes and streams.


# Directions & Parking

From Greens Farms Road; turn left onto Hillspoint Road; pass over US 95; turn left onto Meadow Lane and follow to Grove Point Road; the salt marsh is on the right by the side of the road. Parking is available roadside at the corner of Grove Point Rd and Meadow Lane.
