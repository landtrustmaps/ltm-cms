---
title: Eno Marsh Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.38138, 41.114582 ], [ -73.381335, 41.114403 ], [ -73.380986, 41.114067 ], [ -73.380672, 41.113738 ], [ -73.380022, 41.114146 ], [ -73.379742, 41.114122 ], [ -73.379769, 41.113934 ], [ -73.37985, 41.113344 ], [ -73.37992, 41.112824 ], [ -73.379988, 41.112337 ], [ -73.380696, 41.111658 ], [ -73.380704, 41.111643 ], [ -73.3807, 41.111574 ], [ -73.380694, 41.110932 ], [ -73.380507, 41.110867 ], [ -73.380694, 41.110812 ], [ -73.380805, 41.110627 ], [ -73.380829, 41.110613 ], [ -73.38084, 41.110622 ], [ -73.380853, 41.11063 ], [ -73.380868, 41.110636 ], [ -73.380883, 41.11064 ], [ -73.3809, 41.110642 ], [ -73.380916, 41.110642 ], [ -73.380932, 41.110639 ], [ -73.380948, 41.110635 ], [ -73.380962, 41.110629 ], [ -73.380975, 41.110621 ], [ -73.380995, 41.110603 ], [ -73.381004, 41.110593 ], [ -73.38101, 41.110582 ], [ -73.381014, 41.110571 ], [ -73.381016, 41.110559 ], [ -73.381015, 41.110547 ], [ -73.381011, 41.110536 ], [ -73.381005, 41.110525 ], [ -73.381085, 41.110484 ], [ -73.381085, 41.110484 ], [ -73.381461, 41.110642 ], [ -73.381462, 41.110643 ], [ -73.38183, 41.110799 ], [ -73.382415, 41.111046 ], [ -73.382554, 41.111105 ], [ -73.3832, 41.111378 ], [ -73.380967, 41.111847 ], [ -73.381313, 41.113386 ], [ -73.381282, 41.113463 ], [ -73.381241, 41.113518 ], [ -73.380848, 41.113782 ], [ -73.380901, 41.113833 ], [ -73.382805, 41.113123 ], [ -73.382487, 41.114439 ], [ -73.381938, 41.11473 ], [ -73.38196, 41.11476 ], [ -73.381501, 41.115004 ], [ -73.38138, 41.114582 ] ] ]}}]}
ltmid: '42'
region: westport
size: '14'
image: /dist/assets/41_ENO_MARSH_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/eno-marsh-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.168396,-73.362054&q=49+Weston+Road+Westport,+CT+06880,+United+States&z=12
categories:
  - category: hiking
---
Named for William Phelps Eno who was considered the “Father of traffic safety”, this preserve is mostly wetlands. Eno who is credited with the stop sign, pedestrian crosswalk, and traffic circle had a 32-room mansion located in the area which was demolished in 1997 after a failed attempt to have it moved over water and restored.

An old road located between #278-#282 Saugatuck Ave passes through to a tennis court at #20 Eno Lane. There are some very large trees on either side of the old road. One Tulip tree is wonderfully large with a diameter of about 5 ft. Tulip trees are the tallest tree of eastern forests with the straightest trunks, achieving heights of well over 100 feet with 4 foot diameters and beautiful large tulip- like flowers and tulip-shaped leaves.


# Directions & Parking

Access to the preserve is from the Westport railroad parking area which required a parking permit, or from a private lane between #278 & #282 Saugatuck Ave. Parking is at the end of the drive.
