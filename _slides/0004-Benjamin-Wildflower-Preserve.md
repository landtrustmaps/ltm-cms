---
title: Benjamin Wildflower Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.341913,41.23164],[-73.341813,41.231528],[-73.341734,41.231434],[-73.341649,41.231336],[-73.34157,41.231243],[-73.341517,41.231182],[-73.342181,41.230866],[-73.341826,41.230436],[-73.341717,41.230315],[-73.341916,41.2301],[-73.342226,41.229767],[-73.343302,41.231184],[-73.341998,41.231736],[-73.341913,41.23164]]]}}]}
ltmid: '9'
region: weston
size: '4'
image: /dist/assets/19_BENJAMIN_WILDFLOWER_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/benjamin-wildflower-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.231639,-73.341724&q=30+Old+Stage+Coach+Road+Weston,+CT,+06883,+United+States&z=12
categories:
  - category: hiking
---
A relatively formal garden spot amidst the rugged and random Weston landscape just the place to enjoy some quiet time. The preserve features walking paths and an occasional bench in addition to more than 50 varieties of ferns, wildflowers and trees, mostly native, and all identified and a small boulder field and smaller wet meadow. An airy canopy of tall trees provides a light shade. The display of wildflowers is best in the spring.

# Historical Note:

Weston was first settled in the 1720s. One of the first clusters of permanent dwellings was just south of the preserve along the river. Weston's population grew to about 1,000 people by 1800, and hovered there for 130 years. And then came the Merritt Parkway.

# Directions & Parking

North on Lyons Plain Road; at 3-way stop, bear right and continue up Kellogg Hill Road; left on old Stagecoach Road. Preserve entrance on left. Park along the road and observe the No Parking signs.
