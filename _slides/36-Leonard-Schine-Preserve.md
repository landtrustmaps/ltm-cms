---
title: Leonard Schine Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.366143,41.17256],[-73.367453,41.174261],[-73.365982,41.174802],[-73.366033,41.174992],[-73.364804,41.175821],[-73.362504,41.176121],[-73.362017,41.175526],[-73.361804,41.174996],[-73.362355,41.174626],[-73.363013,41.173578],[-73.363025,41.173281],[-73.363293,41.173029],[-73.363959,41.173709],[-73.363591,41.173813],[-73.364575,41.174959],[-73.365159,41.174861],[-73.366597,41.174117],[-73.365562,41.172477],[-73.366143,41.17256]]]}}]}
ltmid: '36'
region: westport
size: '20'
image: /dist/assets/39_LEONARD_SCHINE_PRESERVE_AND_CHILDRENS_NATURAL_PLAYGROUND.jpg
related: >-
  https://www.aspetucklandtrust.org/leonard-schine-preserve-childrens-natural-playground/
googlemaps: >-
  https://www.google.com/maps?sll=41.1746,-73.364197&q=Glendinning+Place+Westport,+CT,+06880,+United+States&z=12
categories:
  - category: children
  - category: hiking
---
The Land Trusts first preserve, the Leonard Schine Preserve was donated by Ralph Glendinning in 1967 and originally called Twin Bridges Nature Preserve. The preserve was renamed in 1983 to Leonard Schine Preserve. Leonard Schine, one of ALTs founders, was a lawyer who Westport resident Barlow Cutler-Wotton called on to help research and create the Land Trust. Look for some glacial kettles on the eastern portion of the property. Glacial kettles are formed when blocks of ice calve off the front of a glacier and get buried and covered. After the ice melts a hole/depression (kettle) remains.

Natural Playground - This preserve hosts the areas first hands-on natural playground. Officially open in June 2010, the playground, designed for children ages 3-7 and built with natural materials found on the preserve, was inspired by a similar playground at the Minnesota Landscape Arboretum. It comprises approximately 10,000 square feet in a meadow and has areas for fort building, digging, tea-parties, tower-climbing, trail-walking, stick-stacking, nature collages and more! There is even an elvin village in a tree on the property to inspire your child's imagination.  In 2016, we built a bear's den which is a cave built from grape vine, and a giant birds nest which children can add to with sticks. This is a great place to meet for a play date or to bring the grandchildren!

## Directions & Parking: 

Located off of Weston Road (Rt. 57 in Westport). Park on Glendinning Road (private road), which is located 1/10 mile North of Lyons Plains Road (before W. Branch Rd Extension). Look for the preserve sign on the right at roadside parking area.
