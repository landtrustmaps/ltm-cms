---
title: Trout Brook Valley Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.34991,41.256014],[-73.349735,41.257252],[-73.350221,41.2581],[-73.349584,41.258782],[-73.348376,41.25964],[-73.347604,41.261775],[-73.347906,41.262631],[-73.34676,41.262993],[-73.345718,41.263956],[-73.344737,41.265227],[-73.345297,41.266419],[-73.344393,41.267847],[-73.344368,41.268746],[-73.344093,41.269419],[-73.345127,41.270846],[-73.340987,41.273254],[-73.338706,41.274134],[-73.338676,41.274559],[-73.337499,41.27554],[-73.336658,41.276089],[-73.334143,41.27383],[-73.333284,41.273015],[-73.333623,41.272833],[-73.334447,41.272429],[-73.334652,41.270071],[-73.33553,41.270118],[-73.33727,41.270185],[-73.337183,41.269419],[-73.336836,41.269071],[-73.336012,41.268579],[-73.335581,41.268596],[-73.335154,41.267716],[-73.335536,41.267085],[-73.335445,41.266454],[-73.334969,41.265351],[-73.335129,41.264791],[-73.334858,41.264376],[-73.335075,41.263346],[-73.335183,41.262923],[-73.33544,41.26247],[-73.334665,41.261848],[-73.334139,41.260995],[-73.334427,41.260312],[-73.334588,41.258853],[-73.334072,41.2579],[-73.334277,41.256269],[-73.333646,41.255614],[-73.333137,41.255191],[-73.332223,41.25583],[-73.330919,41.256808],[-73.331342,41.257485],[-73.33087,41.257938],[-73.329767,41.256269],[-73.329356,41.255736],[-73.328022,41.251476],[-73.328261,41.249297],[-73.327972,41.248132],[-73.32657,41.246743],[-73.328798,41.245499],[-73.330158,41.246915],[-73.330645,41.246806],[-73.331918,41.24604],[-73.333033,41.247244],[-73.334038,41.246825],[-73.333395,41.246044],[-73.334497,41.245445],[-73.332705,41.24336],[-73.333897,41.242406],[-73.334469,41.242965],[-73.335503,41.242487],[-73.334871,41.241385],[-73.332582,41.242406],[-73.329667,41.243997],[-73.328015,41.241921],[-73.332553,41.23927],[-73.333445,41.240344],[-73.334069,41.240096],[-73.333501,41.238884],[-73.335041,41.238174],[-73.335682,41.239327],[-73.336048,41.239256],[-73.33761,41.241723],[-73.339388,41.244824],[-73.340998,41.245546],[-73.341791,41.245705],[-73.343766,41.245514],[-73.344987,41.244916],[-73.344525,41.242986],[-73.344983,41.242907],[-73.345364,41.242786],[-73.3459,41.242815],[-73.345955,41.244104],[-73.346006,41.244824],[-73.34729,41.245663],[-73.346813,41.246726],[-73.347017,41.248122],[-73.346312,41.249019],[-73.345221,41.249693],[-73.345687,41.253026],[-73.347151,41.253865],[-73.347164,41.254572],[-73.348475,41.255473],[-73.34991,41.256014]]]}}]}
ltmid: '15'
region: easton
region2: weston
size: '1009'
image: /dist/assets/7_TROUT_BROOK_VALLEY_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/trout-brook-valley-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.243745,-73.340881&q=Bradley+Road+Weston,+CT,+06883,+United+States&z=12
categories:
  - category: hiking
  - category: fishing
  - category: children
  - category: birding
---
A crown jewel, Trout Brook Valley (TBV) was saved from developers by concerted citizen action and will now be preserved forever as open space. A largely pristine space, the TBV has almost 14 miles of trails, ranging from easy to difficult. The trails take the hiker past dramatic displays of New England nature at its best, scenic overlooks, and lush Apple and Blueberry orchards. In general, the trails slope upward, sometimes sharply, from the Weston entrance to the Easton entrance off Route 58. Two of the trails are designed for equestrian use. Several trails are linked to the extensive trail systems in the adjoining Jump Hill and Crow Hill Preserves. Also nearby is the Saugatuck Valley Trail on Aquarian Water Company land. Hunting is permitted in limited areas in season by permit only.

Hikers can enjoy a beautiful vista of the Saugatuck Reservoir from the Popp Mountain trail in the Weston Portion of Trout Brook Valley accessible from trail marker #29. The Reservoir was created in 1945, when the river was dammed just north of Devils Glen. What cant be seen is the small village of Valley Forge that now lies 100 feet or so beneath the surface of the Reservoir. In 1760, the first of many iron forges located there, some still operating in the late 19th century. Hike up the steep red trail from the Bradley Road entrance to the lookout at #21 on the orange trail for more stunning views of the valley.

# Directions & Parking

Park at the Bradley Road entrance to access the red trail to Popp Mountain and for the orange trail lookout at #21. 4 main parking areas located on Bradley Road in Weston, and Wells Hill Road, Freeborn Road and Rt 58 in Easton.
