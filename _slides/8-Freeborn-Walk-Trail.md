---
title: Freeborn Walk Trail
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.336261,41.231886],[-73.335524,41.233383],[-73.335594,41.233995],[-73.335506,41.234765],[-73.335427,41.236117],[-73.335802,41.237213],[-73.336253,41.238603],[-73.336358,41.239191],[-73.337081,41.240361],[-73.338007,41.241588],[-73.339687,41.243298],[-73.340257,41.243986],[-73.339911,41.24407],[-73.339424,41.243449],[-73.33771,41.241667],[-73.336807,41.240398],[-73.336052,41.239204],[-73.335909,41.23864],[-73.335472,41.237257],[-73.33509,41.236083],[-73.335215,41.234743],[-73.33532,41.233974],[-73.335239,41.233367],[-73.335731,41.232342],[-73.336108,41.231572],[-73.336261,41.231886]]]}}]}
ltmid: '8'
region: weston
region2: ''
size: '3'
image: /dist/assets/18_FREEBORN_WALK_TRAIL.jpg
related: 'https://www.aspetucklandtrust.org/freeborn-walk-trail/'
googlemaps: >-
  https://www.google.com/maps?sll=41.234996,-73.335362&q=Freeborn+Walk+Trail+Weston,+CT,+06883,+United+States&z=11
categories:
  - category: hiking
---
This preserve provides hikers and equestrians an historical passage to the scenic remoteness and extensive trail systems of the Crow Hill and Trout Brook Valley Preserves. Cut through dense forest perhaps 250 years ago, the abandoned dirt road now threads its bumpy way through recent residential developments as well as forest. The trail is three-quarters of a mile long, rutted, rocky, uphill much of the way, and much like Weston's earliest roads. The trail also provides access to the Lillian Squires Morton preserve through an easement over a driveway. Follow signs at the end of Bradley Road.

# Historical Note:

Weston was well-known in the late 1700s and early 1800s for large stands of Chestnut, Black Walnut, Oak, Basswood and Beech trees, often hundreds of years old. When would-be farmers found the land so unproductive, many turned to harvesting the forest, spurred on by the growing regional markets for railroad ties, bridge flooring, and charcoal.

# Directions & Parking

North on Lyons Plain Road; at 3-way stop, bear right and continue up Kellogg Hill Road; left on old Redding Road. Preserve entrance is on Old Redding Road, north of Kellogg Hill, or enter off Wells Hill Road entrance to Trout Brook Valley.  Trail ends at the dead-end of Bradley Road.
