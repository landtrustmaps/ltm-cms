---
title: Pond View Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.304722, 41.30981 ], [ -73.304969, 41.309777 ], [ -73.305003, 41.309978 ], [ -73.305016, 41.310144 ], [ -73.304849, 41.310242 ], [ -73.305098, 41.310568 ], [ -73.30505, 41.310822 ], [ -73.30496, 41.311026 ], [ -73.304908, 41.31156 ], [ -73.304775, 41.311595 ], [ -73.304157, 41.311732 ], [ -73.304134, 41.311616 ], [ -73.303937, 41.311542 ], [ -73.303614, 41.311443 ], [ -73.303537, 41.311405 ], [ -73.303464, 41.311325 ], [ -73.303453, 41.311195 ], [ -73.303457, 41.311053 ], [ -73.303506, 41.310885 ], [ -73.30356, 41.31079 ], [ -73.303798, 41.310609 ], [ -73.303269, 41.310324 ], [ -73.303489, 41.310142 ], [ -73.303518, 41.309961 ], [ -73.304331, 41.309851 ], [ -73.304372, 41.309833 ], [ -73.304722, 41.30981 ] ] ]}}]}
ltmid: '21'
region: easton
size: '6'
image: /dist/assets/1_POND_VIEW_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/pond-view-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.308249,-73.301484&q=220+Maple+Road+Easton,+CT,+06612,+United+States&z=12
categories:
  - category: hiking
---
Pond View is part of the 128-acre Paine Open Space owned by the town of Easton. Paine Open Space has an extensive trail system that is excellent for year-round hiking and snowshoeing or cross country skiing in winter.

For a nice hike, follow the trails for a short walk around the pond and through a pine grove and open meadow.


# Historic Hay Barn

A classic English Hay Barn on the Paine property was built in 1847.  Today, only the stone foundation remains.  Easton wanted to repair the barn and raised funds to do so but since the building inspector wouldnt let preservationists onto the roof for safety reasons, the restoration was never completed.  The barn was offered to the public, and John Baldwin of Canterbury, Connecticut was awarded the barn by Eastons Board of Selectmen.  Mr. Baldwin owns a home that was built in 1712 and he planned on rebuilding the Paine Barn on the site of an old barn foundation that is on his property.

# Directions & Parking

Follow Stepney Road/ Rt 59 to a left turn onto Judd Road; turn left. Turn left on Maple Road and proceed for one-half mile. Turn right into the main entrance at sign at roadside. Proceed to end of entrance road to trailhead.
