---
title: Kirik Upland Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.305797,41.174927],[-73.304878,41.175193],[-73.304358,41.175409],[-73.303788,41.174596],[-73.30403,41.174232],[-73.30555,41.174056],[-73.305918,41.17405],[-73.30642,41.174112],[-73.306599,41.174547],[-73.305797,41.174927]]]}}]}
ltmid: '28'
region: fairfield
size: '7'
image: /dist/assets/28_KIRIK_UPLAND_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/kirik-upland-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.17249,-73.304178&q=300+Fair+Oak+Drive+Fairfield,+CT,+06824,+United+States&z=12
categories:
  - category: hiking
---
Quiet neighborhood preserve with a field of mature cedar trees (the first trees to grow back in abandoned farm fields), stone walls, and some nice woods.


# Directions & Parking

From Merwyns Lane, turn onto Fair Oaks Drive and park roadside at cul-de-sac. A driveway leads to the preserve, with preserve entrance to the right of the stone pillars. Walk through the Cedar tree field to access a back field and a trail through the woods.
