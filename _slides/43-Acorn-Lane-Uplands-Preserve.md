---
title: Acorn Lane Uplands Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.291489, 41.149104 ], [ -73.291247, 41.148663 ], [ -73.290244, 41.14883 ], [ -73.29003, 41.148882 ], [ -73.28902, 41.149173 ], [ -73.288969, 41.149032 ], [ -73.28887, 41.148874 ], [ -73.289171, 41.148688 ], [ -73.290084, 41.148122 ], [ -73.290296, 41.147991 ], [ -73.290901, 41.148039 ], [ -73.291846, 41.14811 ], [ -73.292234, 41.148807 ], [ -73.291533, 41.149183 ], [ -73.291489, 41.149104 ] ] ]}}]}
ltmid: '33'
region: fairfield
size: '5'
image: /dist/assets/34_ACORN_LANE_UPLAND_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/acorn-lane-upland-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.14746,-73.290278&q=500+Acorn+Lane+Fairfield,+CT,+06890,+United+States&z=12
---
The preserve is mostly uplands with an intermittent stream running though it on the north side. Follow the yellow trail to the much larger Fairfield Mill Hill Open Space (34 acres) which protects the Sasco Creek watershed.

Mill Hill Open Space Maps available at the Town of Fairfield. This is a great preserve to take an adventurous hike in the heart of Fairfield.

## Directions & Parking: 

Acorn Lane is located off of Mill Hill Terrace. Park at the end of the Lane, preserve entrance is marked by a sign at the cul-de-sac.
