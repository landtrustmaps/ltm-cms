---
title: Guard Hill Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.364761, 41.166935 ], [ -73.364822, 41.167439 ], [ -73.36539, 41.167421 ], [ -73.365821, 41.167197 ], [ -73.365866, 41.167173 ], [ -73.365866, 41.167172 ], [ -73.366154, 41.167572 ], [ -73.365182, 41.168074 ], [ -73.363487, 41.168949 ], [ -73.363282, 41.168666 ], [ -73.363279, 41.168662 ], [ -73.363933, 41.167614 ], [ -73.364027, 41.167451 ], [ -73.363933, 41.167317 ], [ -73.364761, 41.166935 ] ] ]}}]}
ltmid: '37'
region: westport
size: '6'
image: /dist/assets/40_GUARD_HILL_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/guard-hill-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.168396,-73.362054&q=49+Weston+Road+Westport,+CT+06880,+United+States&z=12
categories:
  - category: hiking
---
The six-acre Guard Hill Preserve was donated by Adelaide Baker, a poet and author. It consists of a lovely old field with great wildflowers, dogwoods, crabapple, and apple trees. From the field, a trail leads down a hill through some woods to Ford Road.

You can walk down Ford Road on a trail easement to the Leonard Schine preserve pedestrian entrance located at the white trail in back of parking lot. Guard Hill, a high point in Westport, was used as a lookout post when the trees were not there during the Revolutionary War. Below the preserve marks the spot where General Tyron and his troops forded the Saugatuck River in their retreat from the raid on Danbury in 1777 and on their way to the awaiting ships at Compo Beach.


# Directions & Parking

Access to the preserve is from Ford Road or the parking lot behind the Methodist Church (49 Weston Rd/ Rt 57).
