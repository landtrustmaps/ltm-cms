---
title: Harwood Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.279268,41.178022],[-73.278158,41.178905],[-73.276958,41.177521],[-73.276456,41.176824],[-73.276299,41.176441],[-73.27604,41.176236],[-73.276613,41.176013],[-73.277537,41.175692],[-73.27834,41.176426],[-73.278713,41.177052],[-73.279501,41.176887],[-73.27991,41.177655],[-73.279268,41.178022]]]}}]}
ltmid: '26'
region: fairfield
size: '15'
image: /dist/assets/31_HARWOOD_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/harwood-preserve/'
googlemaps: >-
  https://www.google.com/maps/place/700+Lancelot+Rd,+Fairfield,+CT+06824/@41.1790009,-73.2786579,19z/data=!3m1!4b1!4m5!3m4!1s0x89e8055f7d9c15df:0xcfb9779c39799402!8m2!3d41.1790009!4d-73.2781107
categories:
  - category: hiking
---
Harwood Preserve at the end of Lancelot Lane is a 15-acre pocket of hardwood forest, ancient rock walls and small wetland area covered by wooden walkways. There is a short crossover path at the preserve's center which connects the perimeter trails. Deer, crows and wild blueberries (in season) abound at the southerly flank. The trail system begins on the left after one passes a tiny, shallow bog at the entrance to the preserve.


# Directions & Parking

Lancelot Lane is located off of Burr Street. Preserve entrance is at the end of the Lane, parking available roadside in the cul-de-sac.
