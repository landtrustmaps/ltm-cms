---
title: Walter Wagner Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.343196, 41.217464 ], [ -73.343204, 41.217426 ], [ -73.343227, 41.217303 ], [ -73.343252, 41.217181 ], [ -73.343274, 41.217073 ], [ -73.343282, 41.21704 ], [ -73.343292, 41.217008 ], [ -73.343302, 41.216975 ], [ -73.343313, 41.216943 ], [ -73.34332, 41.216926 ], [ -73.343335, 41.216892 ], [ -73.343351, 41.216859 ], [ -73.343368, 41.216826 ], [ -73.343387, 41.216793 ], [ -73.343391, 41.216787 ], [ -73.343409, 41.216758 ], [ -73.343429, 41.21673 ], [ -73.343449, 41.216703 ], [ -73.343471, 41.216676 ], [ -73.343494, 41.216649 ], [ -73.343518, 41.216623 ], [ -73.343527, 41.216613 ], [ -73.34356, 41.216579 ], [ -73.343594, 41.216547 ], [ -73.343629, 41.216515 ], [ -73.343665, 41.216483 ], [ -73.343702, 41.216452 ], [ -73.343719, 41.216438 ], [ -73.34376, 41.216404 ], [ -73.343801, 41.216372 ], [ -73.343843, 41.21634 ], [ -73.343885, 41.216309 ], [ -73.343928, 41.216278 ], [ -73.343962, 41.216255 ], [ -73.343966, 41.216252 ], [ -73.343979, 41.216245 ], [ -73.343993, 41.216239 ], [ -73.344008, 41.216236 ], [ -73.344024, 41.216234 ], [ -73.344039, 41.216234 ], [ -73.344055, 41.216236 ], [ -73.34407, 41.21624 ], [ -73.344084, 41.216245 ], [ -73.344096, 41.216252 ], [ -73.344107, 41.216261 ], [ -73.344116, 41.21627 ], [ -73.344185, 41.216364 ], [ -73.344285, 41.216499 ], [ -73.34436, 41.2166 ], [ -73.344442, 41.216712 ], [ -73.344544, 41.216849 ], [ -73.344626, 41.216959 ], [ -73.344728, 41.217097 ], [ -73.344835, 41.217236 ], [ -73.344836, 41.217237 ], [ -73.344902, 41.21732 ], [ -73.344904, 41.217322 ], [ -73.344946, 41.21737 ], [ -73.34499, 41.217418 ], [ -73.345035, 41.217466 ], [ -73.345082, 41.217513 ], [ -73.345127, 41.217557 ], [ -73.345148, 41.217578 ], [ -73.344238, 41.217851 ], [ -73.343196, 41.217464 ] ] ]}},{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.344834, 41.216958 ], [ -73.344785, 41.216892 ], [ -73.344703, 41.216782 ], [ -73.344601, 41.216645 ], [ -73.344519, 41.216533 ], [ -73.344444, 41.216432 ], [ -73.344344, 41.216297 ], [ -73.344246, 41.216163 ], [ -73.344164, 41.216055 ], [ -73.34411, 41.21598 ], [ -73.344081, 41.215933 ], [ -73.34408, 41.215932 ], [ -73.343997, 41.215811 ], [ -73.343996, 41.215809 ], [ -73.344642, 41.215494 ], [ -73.343962, 41.214597 ], [ -73.343784, 41.213726 ], [ -73.343299, 41.213035 ], [ -73.343917, 41.212761 ], [ -73.343976, 41.212735 ], [ -73.343978, 41.212737 ], [ -73.344277, 41.213155 ], [ -73.344572, 41.213569 ], [ -73.344875, 41.213991 ], [ -73.345116, 41.214329 ], [ -73.345047, 41.21436 ], [ -73.344733, 41.214502 ], [ -73.34491, 41.214744 ], [ -73.345076, 41.215254 ], [ -73.345139, 41.215404 ], [ -73.345376, 41.215963 ], [ -73.345674, 41.216144 ], [ -73.345823, 41.216296 ], [ -73.345603, 41.216432 ], [ -73.345353, 41.216802 ], [ -73.344869, 41.217007 ], [ -73.344834, 41.216958 ] ] ]}}]}
ltmid: '11'
region: weston
size: '13'
image: /dist/assets/20_WALTER_WAGNER_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/walter-wagner-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.216214,-73.344185&q=48+Pheasant+Hill+Road+Weston,+CT,+06883,+United+States&z=13
categories:
  - category: hiking
---
This inviting preserve is located on the eastern rise of the Saugatuck River Valley. The trail is easy and takes about 20 minutes. Along the way, youll follow a meandering creek and see a small pond with gold fish, an old field pasture, a plot of tall bush blueberries, a patch of woodlands, and several spots to relax and soak up the serenity.


# Historical Note:

Serene as the Valley is today, it once was a beehive of manufacturing activity. Strung along the river, and drawing on its power, were several saw and grain mills, a button factory, a tanning operation, a cigar maker, two or three foundries and forges, machine shops, and the Bradley Sharp Edge tool factory, which at one time employed 70 Westonites and shipped its tools to customers around the world.

# Directions & Parking

North on Lyons Plain Road; right on Pheasant Hill Road. Trail entrance at junction of Pheasant Hill and Partridge Lane. Park along either of the roads.
