---
title: Warner Anglers Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.254194,41.241093],[-73.253156,41.240733],[-73.253005,41.24026],[-73.252928,41.239806],[-73.25316,41.239555],[-73.253483,41.240183],[-73.254013,41.240684],[-73.254194,41.241093]]]}},{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.252314,41.237301],[-73.252188,41.237409],[-73.251537,41.237066],[-73.251305,41.236784],[-73.251502,41.236692],[-73.252314,41.237301]]]}},{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.249401,41.232442],[-73.248661,41.231723],[-73.249023,41.23147],[-73.249955,41.231063],[-73.250585,41.230578],[-73.251083,41.230028],[-73.2512,41.229874],[-73.251382,41.230043],[-73.251599,41.230624],[-73.251031,41.231334],[-73.250313,41.231865],[-73.249401,41.232442]]]}}]}
ltmid: '18'
region: easton
size: ''
image: /dist/assets/easton-warner-anglers-1.jpg
related: 'https://www.aspetucklandtrust.org/warner-anglers-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.240889,-73.253312&q=125+South+Park+Avenue+Easton,+CT,+06612,+United+States&z=12
categories:
  - category: fishing
---
This preserve is special because of the Mill River, which in its upper section is a pristine tailwater fishery that flows from the bottom of the Easton Reservoir and ultimately makes its way to Long Island Sound through Southport Harbor in Fairfield, CT. The upper section of the Mill River, which is the portion of the river adjacent to the preserve, is designated by the state as a Class 1 Wild Trout Management Area (WTMA) that by definition means it hosts an abundant population of wild trout and is never stocked. Fishing is permitted year-round with a valid CT license as long as anglers follow strict catch and release guidelines and only use single hook, barb-less lures.

Named after the former president of the Bridgeport Hydraulic (now Aquarion Water Company), Warner Angler's Preserve is divided into three separate parcels all located in Easton, CT and easily accessible from South Park Avenue just north of Exit 47 off the Merritt Parkway
