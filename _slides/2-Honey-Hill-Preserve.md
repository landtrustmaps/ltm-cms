---
title: Honey Hill Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.416063,41.24273],[-73.415617,41.242898],[-73.415728,41.243083],[-73.415226,41.243589],[-73.415014,41.243912],[-73.414013,41.243503],[-73.413108,41.241028],[-73.414726,41.240326],[-73.411971,41.236596],[-73.411466,41.236074],[-73.411287,41.235847],[-73.410962,41.235435],[-73.411057,41.235385],[-73.411081,41.235376],[-73.411333,41.235278],[-73.411536,41.23519],[-73.411857,41.235067],[-73.411952,41.235028],[-73.412019,41.235],[-73.412124,41.234957],[-73.412149,41.234946],[-73.412155,41.234943],[-73.412235,41.234908],[-73.41232,41.234874],[-73.412601,41.23476],[-73.412782,41.234679],[-73.413217,41.235407],[-73.413107,41.235445],[-73.413198,41.235762],[-73.413378,41.236004],[-73.413846,41.236648],[-73.414762,41.236309],[-73.416068,41.235915],[-73.416229,41.236424],[-73.416276,41.236614],[-73.416288,41.237039],[-73.416894,41.237021],[-73.416865,41.236979],[-73.416862,41.236975],[-73.416799,41.236884],[-73.416837,41.236871],[-73.418815,41.236169],[-73.418944,41.236241],[-73.419302,41.236441],[-73.419488,41.236545],[-73.418538,41.236882],[-73.418761,41.236905],[-73.419309,41.23732],[-73.419514,41.237565],[-73.419579,41.237689],[-73.418045,41.238137],[-73.417933,41.238169],[-73.417824,41.238201],[-73.417729,41.238229],[-73.417702,41.238189],[-73.4177,41.238186],[-73.417507,41.238244],[-73.417233,41.238339],[-73.417215,41.238345],[-73.417259,41.238408],[-73.417363,41.238549],[-73.417501,41.238781],[-73.417627,41.238964],[-73.417831,41.239244],[-73.418008,41.239493],[-73.418116,41.239614],[-73.418474,41.240121],[-73.417501,41.240397],[-73.417298,41.240471],[-73.416912,41.240621],[-73.416558,41.240776],[-73.416444,41.240827],[-73.416441,41.240822],[-73.417264,41.241897],[-73.416063,41.24273]]]}}]}
ltmid: '1'
region: weston
size: '86'
image: /dist/assets/11_HONEY_HILL_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/honey-hill-preserve/'
googlemaps: >-
  https://www.google.com/maps?sll=41.232966,-73.414165&q=Wampum+Hill+Road+Weston,+CT,+06883,+United+States&z=12
categories:
  - category: hiking
---
The preserve is an ideal spot for a family walk. The trails are wide and relatively smooth, and the landscape offers a mix of rises, wetlands, and modest outcroppings. Rock walls thread through much of the preserve indicating it was once a collection of woodlots harvested by early settlers to fuel their stoves and fireplaces. The two miles of existing trails have been expanded into the recently-acquired 38-acre Belknap property contiguous acreage which expanded Honey Hill to 119-acres.


# Historical Note:

General Benedict Arnold and 400 Connecticut militia pursued 2,000 British troops through portions of this remote section of Weston's Upper Parish as the Red Coats retreated to Compo Beach in Westport following an ill-fated raid on Danbury in 1777.

# Directions & Parking

Georgetown Road (State Route 57) North; Left on Cannondale Road; Right on Wampum Hill Road; Left on Honey Hill Road; and Left on Mayapple Lane. Park at the end of the lane. The trail entrance is at the end of the paved portion of Wampum Hill Road.
