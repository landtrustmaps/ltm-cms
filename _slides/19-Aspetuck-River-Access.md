---
title: Aspetuck River Access
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[ [ [ -73.332424, 41.201442 ], [ -73.332869, 41.201291 ], [ -73.332885, 41.201316 ], [ -73.332892, 41.201328 ], [ -73.332895, 41.201332 ], [ -73.332931, 41.201382 ], [ -73.332932, 41.201384 ], [ -73.332934, 41.201385 ], [ -73.332948, 41.201404 ], [ -73.332952, 41.20141 ], [ -73.33292, 41.201423 ], [ -73.332376, 41.201641 ], [ -73.332424, 41.201442 ] ] ]}}]}
ltmid: '34'
region: fairfield
size: '0.2'
image: /dist/assets/36_ASPETUCK_RIVER_ACCESS.jpg
related: 'https://www.aspetucklandtrust.org/aspetuck-river-access/'
googlemaps: >-
  https://www.google.com/maps/place/609+Westport+Turnpike,+Fairfield,+CT+06824/@41.2003743,-73.3349598,17z/data=!3m1!4b1!4m5!3m4!1s0x89e804e4d61b75bf:0x3ab6192281a11682!8m2!3d41.2003743!4d-73.3327711
categories:
  - category: hiking
---
Located near the border of Westport & Weston on a small undeveloped parcel directly north of #609 Westport Turnpike on same side of road as #609. The northern boundary is marked by a wooden fence. Park roadisde and walk down to the river. Please note that there is no Aspetuck Land Trust sign on this property. This Fairfield access point to the Aspetuck River provides a great spot for fishing.


# Directions & Parking

Roadside parking on the Aspetuck River side of Route 136.
