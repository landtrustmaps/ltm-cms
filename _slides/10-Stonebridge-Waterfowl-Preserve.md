---
title: Stonebridge Waterfowl Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.389985,41.196273],[-73.388032,41.196932],[-73.387896,41.19664],[-73.386996,41.195341],[-73.386975,41.195264],[-73.387055,41.195218],[-73.388589,41.194573],[-73.388711,41.194576],[-73.38876,41.19463],[-73.389233,41.195239],[-73.389749,41.19595],[-73.389985,41.196273]]]}},{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[-73.386897,41.195053],[-73.388058,41.194488],[-73.387894,41.193714],[-73.387462,41.19412],[-73.386743,41.192786],[-73.386459,41.19289],[-73.38636,41.192928],[-73.385744,41.19303],[-73.385928,41.193531],[-73.386261,41.194179],[-73.386719,41.195042],[-73.386795,41.195077],[-73.386897,41.195053]]]}}]}
ltmid: '10'
region: weston
size: '3'
image: /dist/assets/stonebridge-waterfowl-preserve-weston.jpg
related: 'https://www.aspetucklandtrust.org/stonebridge-waterfowl-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.195247,-73.38669&q=1+Stonebridge+Road+Wilton,+CT,+06897,+United+States&z=13
categories:
  - category: hiking
  - category: fishing
  - category: birding
---
Bring a camera and a fishing pole! The mile-long trail (sometimes rocky) wanders through a rolling meadow and dense woods, skirts marshes, and follows the river. Along the way, there are several excellent fishing holes, a small pond that attracts waterfowl, an unusual stepping stone bridge, small but splashy waterfalls, and vivid displays of marsh iris, loosestrife, and flowering fruit trees.


# Historical Note:

The Stonebridge Waterfowl Preserve was donated to the Aspetuck Land Trust in 1969 by Alice DeLamar, one of the most prominent socialites of her time and a generous patron of the arts who was actively involved in social and environmental causes.<br><br>In 1918, at the age of 23, Alice DeLamar became one of the richest young women in America. She was the daughter of Joseph Delamar, who came to the United States as a stowaway and made his fortune in the mining industry. Alice resided in Weston at 5 Norfield Road in Stonebrook, the former DeLamar estate which exists to this day. Ms. DeLamar also was one of the early owners of the Cobbs Mill Inn in Weston, where in 1936 she converted the inn into one of Westons first fine dining establishments. She was also a close friend of Eva LeGallienne, a well known actor during the first half of the 20th century who donated the LeGallienne Bird Sanctuary to Aspetuck Land Trust. Ms. DeLamar died in 1983 at the age of 88.

# Directions & Parking

Newtown Turnpike (Rt 53) just south of the junction with Norfield Road. Parking is in the fenced area on the west side of the Turnpike. Enter the north parcel trail from the parking area; enter the south parcel trail from Stonebridge Road at trail marker #7.  Dogs must be leashed to protect waterfowl habitat.
