---
title: Taylor Woods Preserve
geojson: >-
  {"type":"FeatureCollection","features":[{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates": [ [ [ -73.338698, 41.198901 ], [ -73.338367, 41.19869 ], [ -73.338203, 41.198586 ], [ -73.338698, 41.198088 ], [ -73.338726, 41.19806 ], [ -73.338632, 41.197936 ], [ -73.338347, 41.19756 ], [ -73.338235, 41.197376 ], [ -73.337915, 41.196895 ], [ -73.337699, 41.196806 ], [ -73.337705, 41.196795 ], [ -73.33771, 41.196782 ], [ -73.337713, 41.196771 ], [ -73.337715, 41.196759 ], [ -73.337715, 41.196757 ], [ -73.337715, 41.196755 ], [ -73.337715, 41.196743 ], [ -73.337713, 41.196728 ], [ -73.33771, 41.196709 ], [ -73.337708, 41.196701 ], [ -73.337707, 41.196698 ], [ -73.337695, 41.196644 ], [ -73.337691, 41.19662 ], [ -73.33769, 41.196614 ], [ -73.33769, 41.19661 ], [ -73.337688, 41.196587 ], [ -73.337687, 41.19655 ], [ -73.337686, 41.196507 ], [ -73.337685, 41.196498 ], [ -73.337682, 41.196456 ], [ -73.337677, 41.196399 ], [ -73.339324, 41.195848 ], [ -73.340604, 41.197651 ], [ -73.340645, 41.19771 ], [ -73.340775, 41.197893 ], [ -73.340863, 41.198018 ], [ -73.340868, 41.198025 ], [ -73.340894, 41.198056 ], [ -73.340917, 41.198084 ], [ -73.340941, 41.198111 ], [ -73.340946, 41.198118 ], [ -73.340974, 41.198152 ], [ -73.340993, 41.198177 ], [ -73.341009, 41.198199 ], [ -73.341025, 41.19822 ], [ -73.34104, 41.198242 ], [ -73.341055, 41.198265 ], [ -73.341069, 41.198286 ], [ -73.34107, 41.198287 ], [ -73.341117, 41.198355 ], [ -73.341118, 41.198357 ], [ -73.34117, 41.198427 ], [ -73.341222, 41.198495 ], [ -73.341281, 41.198572 ], [ -73.34134, 41.198651 ], [ -73.341416, 41.198751 ], [ -73.341492, 41.198855 ], [ -73.341563, 41.198951 ], [ -73.341617, 41.199022 ], [ -73.341669, 41.199091 ], [ -73.341725, 41.199168 ], [ -73.341776, 41.199236 ], [ -73.341832, 41.199311 ], [ -73.341888, 41.199385 ], [ -73.341924, 41.199433 ], [ -73.340239, 41.200091 ], [ -73.339448, 41.199043 ], [ -73.339153, 41.199191 ], [ -73.338698, 41.198901 ] ] ]}}]}
ltmid: '13'
region: weston
size: '22'
image: /dist/assets/22_TAYLOR_WOODS_PRESERVE.jpg
related: 'https://www.aspetucklandtrust.org/taylor-woods-preserve'
googlemaps: >-
  https://www.google.com/maps?sll=41.198428,-73.341261&q=100+Fanton+Hill+Road+Weston,+CT,+06883,+United+States&z=12
---
For a moderately challenging but short hike (45 minutes), this preserve is perfect. Follow the white trail through a patch of woods, then down a slope to the riverbank where huge boulders provide an idyllic spot to sit and enjoy the Aspetuck as it ripples by (fishing is permitted). The trail splits, one leg continuing through woodlands, the other leg turning into a grassy path dotted with berries, honeysuckle, ferns and sassafras. Both legs rejoin and become the red trail leading to the Tall Pines Preserve.

## Directions & Parking: 

Drive north on Lyons Plain Road; take a right on Fanton Hill Road. Parking and trail entrance at end of road.
